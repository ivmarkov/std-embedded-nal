//! DNS implementation based on async_std

use embedded_nal_async::{AddrType, IpAddr};
use std::net::{SocketAddr, ToSocketAddrs};

use crate::Unblock;

/// An std::io::Error compatible error type constructable when to_socket_addrs comes up empty
/// (because it does not produce an error of its own)
#[derive(Debug)]
struct NotFound;

impl core::fmt::Display for NotFound {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(f, "Not found")
    }
}

impl std::error::Error for NotFound {}

/// An std::io::Error compatible error type expressing that a name doesn't fit in the
/// [heapless::String] limits of the requested type
#[derive(Debug)]
struct TooLong;

impl core::fmt::Display for TooLong {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(f, "Name too long")
    }
}

impl std::error::Error for TooLong {}

impl<U> embedded_nal_async::Dns for crate::Stack<U>
where
    U: Unblock,
{
    type Error = std::io::Error;

    async fn get_host_by_name(
        &self,
        hostname: &str,
        addr_type: AddrType,
    ) -> Result<IpAddr, Self::Error> {
        let accept_v4 = addr_type != AddrType::IPv6;
        let accept_v6 = addr_type != AddrType::IPv4;

        // Avoid spawning in a background thread if the hostname is actually an IP address
        if let Ok(addr) = hostname.parse() {
            if matches!(addr, IpAddr::V4(_)) && accept_v4
                || matches!(addr, IpAddr::V6(_)) && accept_v6
            {
                return Ok(addr);
            } else {
                return Err(Self::Error::new(std::io::ErrorKind::NotFound, NotFound));
            }
        }

        let hostname = hostname.to_string();

        self.0
            .unblock(move || {
                // We don't need a port, but the interface of to_socket_addrs (like getaddrinfo) insists on
                // ports being around.
                let fake_port = 1234;

                for addr in (hostname, fake_port).to_socket_addrs()? {
                    match addr {
                        SocketAddr::V4(v) if accept_v4 => {
                            return Ok(v.ip().octets().into());
                        }
                        SocketAddr::V6(v) if accept_v6 => {
                            return Ok(v.ip().octets().into());
                        }
                        _ => continue,
                    }
                }

                Err(Self::Error::new(std::io::ErrorKind::NotFound, NotFound))
            })
            .await
    }

    async fn get_host_by_address(
        &self,
        addr: IpAddr,
        host: &mut [u8],
    ) -> Result<usize, Self::Error> {
        let fakesocketaddr =
            std::net::SocketAddr::new(crate::conversion::IpAddr::from(addr).into(), 1234);

        let (name, _service) = self
            .0
            .unblock(move || dns_lookup::getnameinfo(&fakesocketaddr, 0))
            .await?;

        if name.parse::<std::net::IpAddr>().is_ok() {
            // embedded_nal requires a host name to be returned and is not content with stringified
            // IP addresses
            return Err(Self::Error::new(std::io::ErrorKind::NotFound, NotFound));
        }

        let name = name.as_bytes();

        if name.len() <= host.len() {
            host[..name.len()].copy_from_slice(name);
            Ok(name.len())
        } else {
            // embedded_nal_async cites RFC1035 with "A fully qualified domain name (FQDN), has a
            // maximum length of 255 bytes" -- still, let's not panic just because some system
            // disagrees.
            Err(Self::Error::new(std::io::ErrorKind::OutOfMemory, TooLong))
        }
    }
}
