//! UDP implementation on the standard stack for embedded-nal-async

use async_io::Async;
use embedded_nal_async::IpAddr;
use embedded_nal_async_xtra::Multicast;

use crate::conversion;
use std::io::Error;

use std::net::{Ipv4Addr, Ipv6Addr, UdpSocket};
use std::os::unix::io::AsRawFd;

pub struct ConnectedSocket(Async<std::net::UdpSocket>);
pub struct UniquelyBoundSocket {
    socket: Async<std::net::UdpSocket>,
    // By storing this, we avoid the whole recvmsg hell, which we can because there's really only
    // one relevant address. (Alternatively, we could call `.local_addr()` over and over).
    bound_address: embedded_nal_async::SocketAddr,
}
pub struct MultiplyBoundSocket {
    socket: async_io::Async<std::net::UdpSocket>,
    // Storing this so we can return a full SocketAddr, even though pktinfo doesn't provide that
    // information
    port: u16,
}

impl<U> embedded_nal_async::UdpStack for crate::Stack<U> {
    type Error = Error;
    type Connected = ConnectedSocket;
    type UniquelyBound = UniquelyBoundSocket;
    type MultiplyBound = MultiplyBoundSocket;

    async fn connect_from(
        &self,
        local: embedded_nal_async::SocketAddr,
        remote: embedded_nal_async::SocketAddr,
    ) -> Result<(embedded_nal_async::SocketAddr, Self::Connected), Self::Error> {
        let sock = Async::<std::net::UdpSocket>::bind(std::net::SocketAddr::from(
            conversion::SocketAddr::from(local),
        ))?;

        sock.as_ref()
            .connect(std::net::SocketAddr::from(conversion::SocketAddr::from(
                remote,
            )))?;

        let final_local = sock.as_ref().local_addr()?;

        Ok((
            conversion::SocketAddr::from(final_local).into(),
            ConnectedSocket(sock),
        ))
    }

    async fn bind_single(
        &self,
        local: embedded_nal_async::SocketAddr,
    ) -> Result<(embedded_nal_async::SocketAddr, Self::UniquelyBound), Self::Error> {
        let sock = Async::<std::net::UdpSocket>::bind(std::net::SocketAddr::from(
            conversion::SocketAddr::from(local),
        ))?;

        let final_local = sock.as_ref().local_addr()?;
        let final_local = conversion::SocketAddr::from(final_local).into();

        sock.as_ref().set_broadcast(true)?;

        Ok((
            final_local,
            UniquelyBoundSocket {
                socket: sock,
                bound_address: final_local,
            },
        ))
    }

    async fn bind_multiple(
        &self,
        local: embedded_nal_async::SocketAddr,
    ) -> Result<Self::MultiplyBound, Self::Error> {
        let is_v4 = matches!(&local, embedded_nal_async::SocketAddr::V4(_));

        let sock =
            async_io::Async::<std::net::UdpSocket>::bind(conversion::SocketAddr::from(local))?;

        sock.as_ref().set_broadcast(true)?;

        if is_v4 {
            nix::sys::socket::setsockopt(&sock, nix::sys::socket::sockopt::Ipv4PacketInfo, &true)?;
        } else {
            nix::sys::socket::setsockopt(
                &sock,
                nix::sys::socket::sockopt::Ipv6RecvPacketInfo,
                &true,
            )?;
        }

        let mut local_port = local.port();
        if local_port == 0 {
            local_port = sock.as_ref().local_addr()?.port();
        }

        Ok(MultiplyBoundSocket {
            socket: sock,
            port: local_port,
        })
    }
}

impl embedded_nal_async::ConnectedUdp for ConnectedSocket {
    type Error = Error;

    async fn send(&mut self, data: &[u8]) -> Result<(), Self::Error> {
        let sent_len = self.0.send(data).await?;
        assert!(
            sent_len == data.len(),
            "Datagram was not sent in a single operation"
        );
        Ok(())
    }

    async fn receive_into(&mut self, buffer: &mut [u8]) -> Result<usize, Self::Error> {
        self.0.recv(buffer).await
    }
}

impl embedded_nal_async::UnconnectedUdp for UniquelyBoundSocket {
    type Error = Error;

    async fn send(
        &mut self,
        local: embedded_nal_async::SocketAddr,
        remote: embedded_nal_async::SocketAddr,
        data: &[u8],
    ) -> Result<(), Self::Error> {
        debug_assert!(
            local == self.bound_address,
            "A socket created from bind_single must always provide its original local address (or the one returned from a receive) in send (provided: {local}; bound: {})", self.bound_address
        );
        let remote: std::net::SocketAddr = conversion::SocketAddr::from(remote).into();
        let sent_len = self.socket.send_to(data, remote).await?;
        assert!(
            sent_len == data.len(),
            "Datagram was not sent in a single operation"
        );
        Ok(())
    }

    async fn receive_into(
        &mut self,
        buffer: &mut [u8],
    ) -> Result<
        (
            usize,
            embedded_nal_async::SocketAddr,
            embedded_nal_async::SocketAddr,
        ),
        Self::Error,
    > {
        let (length, remote) = self.socket.recv_from(buffer).await?;
        let remote = conversion::SocketAddr::from(remote).into();
        Ok((length, self.bound_address, remote))
    }
}

impl embedded_nal_async::UnconnectedUdp for MultiplyBoundSocket {
    type Error = Error;

    async fn send(
        &mut self,
        local: embedded_nal_async::SocketAddr,
        remote: embedded_nal_async::SocketAddr,
        data: &[u8],
    ) -> Result<(), Self::Error> {
        if local.port() != 0 {
            debug_assert_eq!(
                local.port(),
                self.port,
                "Packets can only be sent from the locally bound to port"
            );
        }
        let remote: std::net::SocketAddr = conversion::SocketAddr::from(remote).into();
        match remote {
            // The whole cases are distinct as send_msg is polymorphic
            std::net::SocketAddr::V6(remote) => {
                // Taking this step on foot due to https://github.com/nix-rust/nix/issues/1754
                let remote = nix::sys::socket::SockaddrIn6::from(remote);
                let local_pktinfo = conversion::IpAddr::from(local.ip()).into();
                let control = [nix::sys::socket::ControlMessage::Ipv6PacketInfo(
                    &local_pktinfo,
                )];
                self.socket
                    .write_with(|s| {
                        let sent_len = nix::sys::socket::sendmsg(
                            s.as_raw_fd(),
                            &[std::io::IoSlice::new(data)],
                            // FIXME this ignores the IP part of the local address
                            &control,
                            nix::sys::socket::MsgFlags::empty(),
                            Some(&remote),
                        )?;
                        assert!(
                            sent_len == data.len(),
                            "Datagram was not sent in a single operation"
                        );
                        Ok(())
                    })
                    .await
            }
            std::net::SocketAddr::V4(remote) => {
                // Taking this step on foot due to https://github.com/nix-rust/nix/issues/1754
                let remote = nix::sys::socket::SockaddrIn::from(remote);
                let local_pktinfo = conversion::IpAddr::from(local.ip()).into();
                let control = [nix::sys::socket::ControlMessage::Ipv4PacketInfo(
                    &local_pktinfo,
                )];
                self.socket
                    .write_with(|s| {
                        let sent_len = nix::sys::socket::sendmsg(
                            s.as_raw_fd(),
                            &[std::io::IoSlice::new(data)],
                            // FIXME this ignores the IP part of the local address
                            &control,
                            nix::sys::socket::MsgFlags::empty(),
                            Some(&remote),
                        )?;
                        assert!(
                            sent_len == data.len(),
                            "Datagram was not sent in a single operation"
                        );
                        Ok(())
                    })
                    .await
            }
        }
    }

    async fn receive_into(
        &mut self,
        buffer: &mut [u8],
    ) -> Result<
        (
            usize,
            embedded_nal_async::SocketAddr,
            embedded_nal_async::SocketAddr,
        ),
        Self::Error,
    > {
        let (length, remote, local) = self.socket.read_with(|s| {
            let mut iov = [std::io::IoSliceMut::new(buffer)];
            let mut cmsg = nix::cmsg_space!(nix::libc::in6_pktinfo);
            let received = nix::sys::socket::recvmsg(
                s.as_raw_fd(),
                &mut iov,
                Some(&mut cmsg),
                nix::sys::socket::MsgFlags::MSG_TRUNC,
                )
                .map_err(Error::from)?;
            let local = match received.cmsgs().next() {
                Some(nix::sys::socket::ControlMessageOwned::Ipv6PacketInfo(pi)) => {
                    embedded_nal_async::SocketAddr::new(
                        conversion::IpAddr::from(pi).into(),
                        self.port,
                        )
                },
                Some(nix::sys::socket::ControlMessageOwned::Ipv4PacketInfo(pi)) => {
                    embedded_nal_async::SocketAddr::new(
                        conversion::IpAddr::from(pi).into(),
                        self.port,
                        )
                },
                _ => panic!("Operating system failed to send IPv4/IPv6 packet info after acknowledging the socket option")
            };
            Ok((received.bytes, received.address, local))
        }).await?;

        let remote: nix::sys::socket::SockaddrStorage =
            remote.expect("recvmsg on UDP always returns a remote address");
        // Taking this step on foot due to https://github.com/nix-rust/nix/issues/1754
        let remote = match (remote.as_sockaddr_in6(), remote.as_sockaddr_in()) {
            (Some(remote), None) => std::net::SocketAddr::V6(std::net::SocketAddrV6::new(
                remote.ip(),
                remote.port(),
                remote.flowinfo(),
                remote.scope_id(),
            )),
            (None, Some(remote)) => std::net::SocketAddr::V4(std::net::SocketAddrV4::new(
                remote.ip().into(),
                remote.port(),
            )),
            _ => panic!("Unexpected address type"),
        };

        // We could probably shorten things by going more directly from SockaddrLike
        let remote = conversion::SocketAddr::from(remote).into();
        Ok((length, local, remote))
    }
}

impl Multicast for UniquelyBoundSocket {
    type Error = Error;

    async fn join(&mut self, multicast_addr: IpAddr) -> Result<(), Self::Error> {
        match multicast_addr {
            IpAddr::V4(addr) => join_multicast_v4(
                self.socket.get_ref(),
                addr.octets().into(),
                std::net::Ipv4Addr::UNSPECIFIED,
            )?,
            IpAddr::V6(addr) => join_multicast_v6(self.socket.get_ref(), addr.octets().into(), 0)?,
        }

        Ok(())
    }

    async fn leave(&mut self, multicast_addr: IpAddr) -> Result<(), Self::Error> {
        match multicast_addr {
            IpAddr::V4(addr) => leave_multicast_v4(
                self.socket.get_ref(),
                addr.octets().into(),
                std::net::Ipv4Addr::UNSPECIFIED,
            )?,
            IpAddr::V6(addr) => leave_multicast_v6(self.socket.get_ref(), addr.octets().into(), 0)?,
        }

        Ok(())
    }
}

impl Multicast for MultiplyBoundSocket {
    type Error = Error;

    async fn join(&mut self, multicast_addr: IpAddr) -> Result<(), Self::Error> {
        match multicast_addr {
            IpAddr::V4(addr) => join_multicast_v4(
                self.socket.get_ref(),
                addr.octets().into(),
                std::net::Ipv4Addr::UNSPECIFIED,
            )?,
            IpAddr::V6(addr) => join_multicast_v6(self.socket.get_ref(), addr.octets().into(), 0)?,
        }

        Ok(())
    }

    async fn leave(&mut self, multicast_addr: IpAddr) -> Result<(), Self::Error> {
        match multicast_addr {
            IpAddr::V4(addr) => leave_multicast_v4(
                self.socket.get_ref(),
                addr.octets().into(),
                std::net::Ipv4Addr::UNSPECIFIED,
            )?,
            IpAddr::V6(addr) => leave_multicast_v6(self.socket.get_ref(), addr.octets().into(), 0)?,
        }

        Ok(())
    }
}

fn join_multicast_v6(socket: &UdpSocket, multiaddr: Ipv6Addr, interface: u32) -> Result<(), Error> {
    socket.join_multicast_v6(&multiaddr, interface)?;

    //info!("Joined IPV6 multicast {}/{}", multiaddr, interface);

    Ok(())
}

fn leave_multicast_v6(
    socket: &UdpSocket,
    multiaddr: Ipv6Addr,
    interface: u32,
) -> Result<(), Error> {
    socket.leave_multicast_v6(&multiaddr, interface)?;

    //info!("Left IPV6 multicast {}/{}", multiaddr, interface);

    Ok(())
}

fn join_multicast_v4(
    socket: &UdpSocket,
    multiaddr: Ipv4Addr,
    interface: Ipv4Addr,
) -> Result<(), Error> {
    #[cfg(not(target_os = "espidf"))]
    socket.join_multicast_v4(&multiaddr, &interface)?;

    // join_multicast_v4() is broken for ESP-IDF, most likely due to wrong `ip_mreq` signature in the `libc` crate
    // Note that also most *_multicast_v4 and *_multicast_v6 methods are broken as well in Rust STD for the ESP-IDF
    // due to mismatch w.r.t. sizes (u8 expected but u32 passed to setsockopt() and sometimes the other way around)
    #[cfg(target_os = "espidf")]
    {
        fn esp_setsockopt<T>(
            socket: &UdpSocket,
            proto: u32,
            option: u32,
            value: T,
        ) -> Result<(), Error> {
            use std::os::fd::AsRawFd;

            esp_idf_sys::esp!(unsafe {
                esp_idf_sys::lwip_setsockopt(
                    socket.as_raw_fd(),
                    proto as _,
                    option as _,
                    &value as *const _ as *const _,
                    core::mem::size_of::<T>() as _,
                )
            })
            .map_err(|_| ErrorCode::StdIoError)?;

            Ok(())
        }

        let mreq = esp_idf_sys::ip_mreq {
            imr_multiaddr: esp_idf_sys::in_addr {
                s_addr: u32::from_ne_bytes(multiaddr.octets()),
            },
            imr_interface: esp_idf_sys::in_addr {
                s_addr: u32::from_ne_bytes(interface.octets()),
            },
        };

        esp_setsockopt(
            socket,
            esp_idf_sys::IPPROTO_IP,
            esp_idf_sys::IP_ADD_MEMBERSHIP,
            mreq,
        )?;
    }

    //info!("Joined IP multicast {}/{}", multiaddr, interface);

    Ok(())
}

fn leave_multicast_v4(
    socket: &UdpSocket,
    multiaddr: Ipv4Addr,
    interface: Ipv4Addr,
) -> Result<(), Error> {
    socket.leave_multicast_v4(&multiaddr, &interface)?;

    Ok(())
}
